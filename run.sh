#!/bin/bash

./socat.sh &

MAC_IP=$(ifconfig en0 | grep inet | awk '$1=="inet" {print $2}')
docker run --rm -e DISPLAY=$MAC_IP:0 xeyes
